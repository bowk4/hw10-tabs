document.addEventListener('DOMContentLoaded', function() {

    const tabLink = document.querySelectorAll('.tabs-title')
    const tabItem = document.querySelectorAll('.tab-item')

    tabItem.forEach(elem => elem.style.display = 'none')

    tabLink.forEach(link => {
        link.addEventListener('click', () => {

            const tab = link.dataset.tab;

            tabItem.forEach(item => item.style.display = 'none');
            document.getElementById(tab).style.display = 'block';

            tabLink.forEach(link => link.classList.remove('active'));
            link.classList.add('active');
        })
    })
})
